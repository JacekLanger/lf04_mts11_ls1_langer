package loops;

import java.util.ArrayList;
import java.util.List;

public class Primesive {

  /**
   * sift first x numbers and check if it's a prime number;
   *
   * @param number
   */
  public static void siveFirst(int number) {

    // list for primes
    List<Integer> primes = new ArrayList<>();
    // list for all other
    List<Integer> multiples = new ArrayList<>();
    // first prime num
    primes.add(2);

    // number can only divide by itself or 1
    for (int i = 2; i <= number; i++) {
      // bool for tracking if it's a prime
      boolean isPrime = true;

      // if multiples of prime we don't need to do any checks
      if (!multiples.contains(i)) {
        for (Integer prime : primes) {
          if (i % prime == 0) {
            isPrime = false;
          }
        }
      }
      if (isPrime) {
        primes.add(i);
        // add all multiples of the prime to the primes
        for (int j = 0; j < number; j++) {
          final int num = j;
          primes.forEach(prime -> multiples.add(prime * num));
        }
      }
    }
    print(primes);
  }

  private static void print(List<Integer> primes) {

    StringBuilder sb = new StringBuilder().append("primes:");

    primes.forEach(p -> sb.append(p).append(", "));
    System.out.println(sb.toString());
  }

}

