package loops;

public class LoopTasks {

  public static void main(String[] args) {

//    aufgabeVier(99);
    Primesive.siveFirst(200);
  }

  public static void aufgabeVier(int number) {

    System.out.println("\n##############\n");
    while (number > 0) {
      print(number);
      number -= 3;
    }

    System.out.println("\n##############\n");
    for (int i = 0; i < 20; i++) {
      print(i * i);
    }
    int num = 2;

    System.out.println("\n##############\n");
    do {
      print(num += 4);
    } while (num < 100);

    System.out.println("\n##############\n");
    for (int i = 0; i < 20; i++) {
      print((int) Math.pow(2, i));
    }
  }


  private static void print(int num) {

    System.out.print(num);
    System.out.print(",");
  }

}
