import java.util.Scanner;

/**
 * Fahrkartenautomat logic.
 */
public class Fahrkartenautomat {

  /**
   * fare prices.
   */
  private static final double[] fahrkartenPreise =
      { 2.9, 3.3, 3.6, 1.9, 8.6, 9, 9.6, 23.5, 24.3, 24.9 };
  /**
   * fare names.
   */
  private static final String[] fahrkarten = { "Einzelfahrschein Berlin AB",
                                               "Einzelfahrschein Berlin BC",
                                               "Einzelfahrschein Berlin ABC",
                                               "Kurzstrecke",
                                               "Tageskarte Berlin AB",
                                               "Tageskarte Berlin BC",
                                               "Tageskarte Berlin ABC",
                                               "Kleingruppen-Tageskarte Berlin AB",
                                               "Kleingruppen-Tageskarte Berlin BC",
                                               "Kleingruppen-Tageskarte Berlin ABC"
  };

  /**
   * entry point.
   *
   * @param args arguments
   */
  public static void main(String... args) {

    do {
      double eingeworfeneMuenze;
      double rueckgabebetrag;
      Scanner tastatur = new Scanner(System.in);
      double zuZahlenderBetrag = fahrkartenbestellungErfassen(tastatur);

      double eingezahlterGesamtbetrag = 0.0;

      eingezahlterGesamtbetrag = eingezahlterGesamtbetragBerechnen(tastatur,
                                                                   zuZahlenderBetrag,
                                                                   eingezahlterGesamtbetrag);
      // Fahrscheinausgabe
      // -----------------
      fahrscheineDrucken();
      muenzeAusgeben(zuZahlenderBetrag, eingezahlterGesamtbetrag);
    } while (true);
  }

  /**
   * Prints a welcome message and takes user input to process.
   *
   * @param scanner user input
   * @return double value parsed from user input
   */
  private static double bestellungErfassung(Scanner scanner) {

    StringBuilder sb = new StringBuilder();
    double zuZahlenderBetrag = 0.0;
    System.out.print("Bitte wählen sie Fahrkarte aus: \n\n");

    return zuZahlenderBetrag;
  }

  /**
   * gets the amount of tickets ordered, if the amount is 0 the Program terminates, if the
   * amount is above 10 the customer is beeing asked if he wants to amend his input or continue
   * with 1 ticket.
   *
   * @param tastatur the input scanner
   * @return the amount of tickets orded
   */
  private static double fahrkartenbestellungErfassen(Scanner tastatur) {

    System.out.println("Bitte wählen sie eine Fahrkarte aus.");

    for (int i = 0; i < fahrkarten.length; i++) {
      System.out.printf("(%d) %s : %.2f €\n", i + 1, fahrkarten[i], fahrkartenPreise[i]);
    }

    int input = tastatur.nextInt();
    int index =
        // if input is > 0 check if input is < fahrkartenPreise.length if false set to max length
        input > 0 ? input <= fahrkartenPreise.length ? input - 1
                    // input is allways one higher than the actual input
                                                     : fahrkartenPreise.length : 0;

    double fahrkartenPreis = fahrkartenPreise[index];

    int anzahlDerFahrscheine = 0;
    System.out.print("Wieviele Tickets möchten Sie lösen? Zum abbrechen 0 eingeben: ");
    try {
      anzahlDerFahrscheine = tastatur.nextInt();
      if (anzahlDerFahrscheine > 10) {
        System.out.println("Es können maximal 10 Fahrscheine auf einmal gelöst werden. "
                               + "Wollen Sie mit 10 Fahrscheinen fortfahren? (Y/n)");
        anzahlDerFahrscheine = 10;
        if ("n".equalsIgnoreCase(tastatur.next())) {
          fahrkartenbestellungErfassen(tastatur);
        }
      }
      if (anzahlDerFahrscheine < 0) {
        System.out.println(
            "Es muss mindestens ein Fahrschein " +
                "bestellt werden. zum abbrechen 0 eingeben");
        fahrkartenbestellungErfassen(tastatur);
      }
      if (anzahlDerFahrscheine == 0) {
        System.out.println("Programm bricht ab!");
        main();
      }
    } catch (NumberFormatException e) {
      e.printStackTrace();
      System.out.println("geben sie bitte eine ganze zahl ein");
      fahrkartenbestellungErfassen(tastatur);
    }
    return anzahlDerFahrscheine * fahrkartenPreis;
  }

  /**
   * Calculate the amount that was paid already.
   *
   * @param tastatur                 th input scanner
   * @param zuZahlenderBetrag        the amount that is to be payed
   * @param eingezahlterGesamtbetrag the amount that was payed
   * @return the exces that was payed to much and will be returned to the customer
   */
  private static double eingezahlterGesamtbetragBerechnen(Scanner tastatur,
                                                          double zuZahlenderBetrag,
                                                          double eingezahlterGesamtbetrag) {

    double eingeworfeneMuenze;
    while (eingezahlterGesamtbetrag < zuZahlenderBetrag) {
      System.out.printf("Noch zu zahlen: %.2f EURO \n", (zuZahlenderBetrag -
                                                             eingezahlterGesamtbetrag));

      // get the highest value bill that is accepted for the price.
      double bill = getBillValue(zuZahlenderBetrag - eingezahlterGesamtbetrag);
      System.out.printf("Eingabe (mind. 5Ct, höchstens %s Euro):", bill);

      double eingeworfen = tastatur.nextDouble();


      eingeworfen = validateBills(eingeworfen, bill);
      eingeworfeneMuenze = eingeworfen;

      eingezahlterGesamtbetrag += eingeworfeneMuenze;
    }
    if (zuZahlenderBetrag - eingezahlterGesamtbetrag > 0 &&
        zuZahlenderBetrag - eingezahlterGesamtbetrag < 0.01) {
      eingezahlterGesamtbetrag = zuZahlenderBetrag;
    }
    return eingezahlterGesamtbetrag;
  }

  /**
   * Check if provided value is valid, only values of 50, 20, 10, 5, 2, 1, 0.5, 0.2, 0.1, 0.5 are
   * valid
   *
   * @param eingeworfen amount recived
   * @param bill        valid amount
   * @return the amount recived if valid else 0
   */
  private static double validateBills(double eingeworfen, double bill) {

    double value = 0;
    if (eingeworfen > 0) {
      if (bill < 5) {
        if (bill > 1) {
          value = eingeworfen % 2 == 0 ? eingeworfen : eingeworfen % 1 == 0 ? eingeworfen : 0;
        }

        value = eingeworfen % 0.5 == 0 ? eingeworfen :
                eingeworfen % 0.2 == 0 ? eingeworfen :
                eingeworfen % 0.1 == 0 ? eingeworfen :
                eingeworfen % 0.05 == 0 ? eingeworfen : 0;

      } else if (bill > 5) {
        value = switch ((int) eingeworfen) {
          case 5 -> 5;
          case 10 -> 10;
          case 20 -> 20;
          case 50 -> 50;
          default -> 0;
        };
      }
    }
    // check if the sum is valid for the amount to be paid
    value = value > bill ? 0 : value;
    if (value == 0) {
      System.out.println("eingegebener Betrag nicht zulässig");
    }
    return value;
  }

  /**
   * Returns the bill value depending on the remaining price to pay.
   *
   * @param zuZahlenderBetrag remaining price to pay
   * @return bill value
   */
  private static double getBillValue(double zuZahlenderBetrag) {

    double bills;
    if (zuZahlenderBetrag > 30) {
      bills = 50;
    } else if (zuZahlenderBetrag > 10) {
      bills = 20;
    } else if (zuZahlenderBetrag > 1) {
      bills = 10;
    } else {
      bills = 2;
    }
    return bills;
  }

  /**
   * print the tickets.
   */
  private static void fahrscheineDrucken() {

    System.out.println("\nFahrschein wird ausgegeben");
    for (int i = 0; i < 8; i++) {
      System.out.print("=");
      try {
        warte(250);
      } catch (InterruptedException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }
    System.out.println("\n\n");
  }

  /**
   * wait for a certain duration before proceding.
   *
   * @param millis milliseconds to wait
   * @throws InterruptedException if a cancelation token is recived
   */
  private static void warte(int millis) throws InterruptedException {

    Thread.sleep(millis);
  }

  /**
   * Print the change in valid steps, where a valid step equals a coin value of the EURO
   * curruency.
   *
   * @param zuZahlenderBetrag        amount to be payed
   * @param eingezahlterGesamtbetrag amount payed
   */
  private static void muenzeAusgeben(double zuZahlenderBetrag,
                                     double eingezahlterGesamtbetrag) {

    double rueckgabebetrag;
    // Rückgeldberechnung und -Ausgabe
    // -------------------------------
    rueckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;

    if (rueckgabebetrag > 0.0) {
      System.out.printf(
          "Der Rückgabebetrag in Höhe von %.2f EURO \nwird in folgenden Münzen " +
              "ausgezahlt:\n\n",
          rueckgabebetrag);
      while (rueckgabebetrag >= 2.0) // 2 EURO-Münzen
      {
        rueckgabebetrag -= muenzeAusgeben(2, "EURO");
      }
      while (rueckgabebetrag >= 1.0) // 1 EURO-Münzen
      {
        rueckgabebetrag -= muenzeAusgeben(1, "EURO");
      }
      while (rueckgabebetrag >= 0.5) // 50 CENT-Münzen
      {
        rueckgabebetrag -= muenzeAusgeben(50, "CENT");
      }
      while (rueckgabebetrag >= 0.2) // 20 CENT-Münzen
      {
        rueckgabebetrag -= muenzeAusgeben(20, "CENT");
      }
      while (rueckgabebetrag >= 0.1) // 10 CENT-Münzen
      {
        rueckgabebetrag -= muenzeAusgeben(10, "CENT");
      }
      while (rueckgabebetrag >= 0.01)// 5 CENT-Münzen
      {
        rueckgabebetrag -= muenzeAusgeben(5, "CENT");
      }
    }
    System.out.println("\nVergessen Sie nicht, den Fahrschein\n" +
                           "vor Fahrtantritt entwerten zu lassen!\n" +
                           "Wir wünschen Ihnen eine gute Fahrt.");

    try {
      warte(1000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  /**
   * return coins to the customer.
   *
   * @param betrag  the coin value
   * @param einheit the coin currency
   * @return the value that is beeing returned
   */
  private static double muenzeAusgeben(int betrag, String einheit) {

    double realBetrag = betrag;

    if (einheit.equals("CENT")) {
      realBetrag *= 0.01;
    }

    System.out.printf("%d %s\n", betrag, einheit);

    return realBetrag;
  }

}
